rps = ["rock", "paper", "scissors"]
outcomes = ["draw", "win", "lose"]

# considering index
# if A is 1 more than B in the cycle, A beats B (A wins)
# if 1 less (-1 % 3 = 2), B beats A (A loses)
# if equal, draw

outcome_for_a = lambda a_rps, b_rps: (a_rps - b_rps) % 3
b_for_outcome_for_a = lambda a_rps, a_out: (a_rps - a_out) % 3

# Testing
print("Testing A vs B outcomes")
for ai, an in enumerate(rps):
    for bi, bn in enumerate(rps):
        oi = outcome_for_a(ai, bi)
        print("{} (A) vs {} (B) -> A {}s".format(an, bn, outcomes[oi]))

print("Testing A and needed B for a certain outcome")
for ai, an in enumerate(rps):
    for oi, on in enumerate(outcomes):
        bi = b_for_outcome_for_a(ai, oi)
        print("For {} (A) to {} -> B: {}".format(an, on, rps[bi]))
